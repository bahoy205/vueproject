import Vue from "vue";
import VueRouter from "vue-router";
import HomeView from "../views/HomeView.vue";
// import Simpale from "../views/Simpale.vue";
import Grade from "../views/Grade.vue";
// import Test from "../views/Test.vue";
// import Apicon from "../views/Apicon.vue";
import Login from "../views/Login.vue";
import ProductDetail from "../views/ProductDetail.vue";
import Cart from "../views/Cart.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/cart",
    name: "Cart",
    component: Cart,
  },
  {
    path: "",
    name: "toolbar",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Toolbar.vue"),
    children: [
      // {
      //   path: "/about",
      //   name: "about",

      //   component: () =>
      //     import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
      // },
      {
        path: "/me",
        name: "me",

        component: () =>
          import(/* webpackChunkName: "about" */ "../views/Me.vue"),
      },
      // {
      //   path: "/simpale",
      //   name: "simpale",

      //   component: Simpale,
      // },
      {
        path: "/grade",
        name: "grade",

        component: Grade,
      },
      // {
      //   path: "/test",
      //   name: "test",

      //   component: Test,
      // },
      // {
      //   path: "/apicon",
      //   name: "apicon",

      //   component: Apicon,
      // },
    
      {
        path: "/productdetail",
        name: "productdetail",
        
        component: ProductDetail,
      },
      
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
